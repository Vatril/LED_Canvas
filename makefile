# Name: Makefile
# Author: Nikolai Jay Summers + holachek
# License: GNU 3

#taken and modified from https://gist.github.com/holachek/3304890

# DEVICE ....... The AVR device you compile for
# CLOCK ........ Target AVR clock rate in Hertz
# OBJECTS ...... The object files created from your source files. This list is
#                usually the same as the list of source files with suffix ".o".
# PROGRAMMER ... Options to avrdude which define the hardware you use for
#                uploading to the AVR and the interface where this hardware
#                is connected.
# FUSES ........ Parameters for avrdude to flash the fuses appropriately.

DEVICE     = attiny85
CLOCK      = 8000000
PROGRAMMER = -c arduino -P /dev/ttyUSB0 -b 19200
OBJECTS    = ledcanvas.o
FUSES      = -U lfuse:w:0x62:m -U hfuse:w:0xdf:m -U efuse:w:0xff:m


######################################################################
######################################################################


# Tune the lines below only if you know what you are doing:

AVRDUDE = avrdude $(PROGRAMMER) -p $(DEVICE)
COMPILE = avr-gcc -Wall -O3 -DF_CPU=$(CLOCK) -mmcu=$(DEVICE)

# symbolic targets:
all:	ledcanvas.hex

.c.o:
	$(COMPILE) -c $< -o $@

.S.o:
	$(COMPILE) -x assembler-with-cpp -c $< -o $@
# "-x assembler-with-cpp" should not be necessary since this is the default
# file type for the .S (with capital S) extension. However, upper case
# characters are not always preserved on Windows. To ensure WinAVR
# compatibility define the file type manually.

.c.s:
	$(COMPILE) -S $< -o $@

flash:	all
	$(AVRDUDE) -U flash:w:ledcanvas.hex:i

fuse:
	$(AVRDUDE) $(FUSES)

install: flash fuse

# if you use a bootloader, change the command below appropriately:
load: all
	bootloadHID ledcanvas.hex

clean:
	rm -f ledcanvas.hex ledcanvas.elf $(OBJECTS)

# file targets:
ledcanvas.elf: $(OBJECTS)
	$(COMPILE) -o ledcanvas.elf $(OBJECTS)

ledcanvas.hex: ledcanvas.elf
	rm -f ledcanvas.hex
	avr-objcopy -j .text -j .data -O ihex ledcanvas.elf ledcanvas.hex
# If you have an EEPROM section, you must also create a hex file for the
# EEPROM and add it to the "flash" target.

# Targets for code debugging and analysis:
disasm:	ledcanvas.elf
	avr-objdump -d ledcanvas.elf

cpp:
	$(COMPILE) -E ledcanvas.c

#include <avr/io.h>
#include <avr/interrupt.h>
#include "ledcanvas.h"


//#################### CONFIGURE HERE

/**
* Configure the mode of all 32 LEDs here.
* Even if you have less, just set the unused ones to OFF then.
*
* OFF = LED is off
* ON = LED is on at a given intensity
* FULLWAVE = LED pulses on and off and starts at the given intensity
* VARIATE = LED pulses but never drops below LEDMIN and starts at the given intensity
* HIGHLIGHT = LED randomly pulses, is off at all other times
*/
LEDmode states[32] = {
  HIGHLIGHT,HIGHLIGHT,HIGHLIGHT,HIGHLIGHT,HIGHLIGHT,HIGHLIGHT,HIGHLIGHT,HIGHLIGHT,
  ON,ON,ON,ON,ON,ON,ON,ON,
  OFF,FULLWAVE,OFF,FULLWAVE,OFF,FULLWAVE,OFF,FULLWAVE,
  VARIATE,VARIATE,VARIATE,VARIATE,VARIATE,VARIATE,VARIATE,VARIATE
};

/**
* An array of the starting intensity of each LED.
* Must remain 32 elements long.
* 0 if off 2^STEP is maximum
*/
unsigned char ledLight[32] = {
  16,16,16,16,16,16,16,16,
  8,0,16,5,16,15,1,2,
  16,8,16,16,16,5,16,12,
  16,15,14,13,12,11,10,9
};

/**
* How many steps a cycle has.
* Actual steps will be 2^STEP
* The more steps, the more the LEDs will flicker
*/
#define STEPS 4

/**
* The minimum intensity a VARIATE LED will drop to.
*/
#define LEDMIN 8

/**
* A value that is used in the randomizer to set how often it should trigger.
* it should be in the range of an unsigned char.
*/
#define RANDOMSPEED 150

/**
* How fast the LEDs should dimm when they are pulsing.
*/
#define DIMMSPEED 2


//######################

/**
* The tick is used to count the TIMER0_COMPA matches.
*/
volatile unsigned char tick = 0;

/**
* The seed for the pseudo random generator.
*/
long long seed = 1233548624113657L;

/**
* The ledTick is used to keep track where in the cycle the LEDs are.
*/
unsigned char ledTick = 0;

/**
* An array to remeber states about the LEDs, used to calculate the next.
*/
unsigned char ledcontrol[32];


/**
* Counts tick up whenever it there is a compare match a on timer 0
*/
ISR(TIMER0_COMPA_vect){
  tick++;
}

/**
* The main function.
* Sets everything up and has the event loop.
*/
int main(void){
  char saveSREG;
  InitOutputs();
  InitTimer();
  CheckConfig();
  while(1){
    sei();
    if(tick > 0){
      saveSREG = SREG;
      cli ();
      tick--;
      SREG = saveSREG;
      WriteState(GenerateNextState(++ledTick));
      if(ledTick > (1<<STEPS))
        ledTick = 0;
    }
  }
  return 0;
}

/**
* Generates the next LEDstate given an ledTick
*/
LEDstate GenerateNextState(unsigned char ltick){
  LEDstate temp = 0;
  for(unsigned char i = 0; i < 32; i++){
    switch (states[i]) {
      case OFF:
        break;
      case ON:
        if(ltick < ledLight[i]){
          temp |= 1;
        }
        break;
      case FULLWAVE:
        if(ltick < ledLight[i]){
          temp |= 1;
        }
        if(ltick == 1){
          ledcontrol[i]++;
          if(ledcontrol[i] & (1<<DIMMSPEED)){
            ledcontrol[i] &= ~63;
            ledLight[i] += (ledcontrol[i] & 128)?1:-1;
            if(ledLight[i] & (1<<STEPS) || !ledLight[i]){
              ledcontrol[i] ^= 128;
            }
          }
        }
        break;
      case VARIATE:
      if(ltick < ledLight[i]){
        temp |= 1;
      }
      if(ltick == 1){
        ledcontrol[i]++;
        if(ledcontrol[i] & (1<<DIMMSPEED)){
          ledcontrol[i] &= ~63;
          ledLight[i] += (ledcontrol[i] & 128)?1:-1;
          if(ledLight[i] & (1<<STEPS) || (ledLight[i] < LEDMIN)){
            ledcontrol[i] ^= 128;
          }
        }
      }
      break;
      case HIGHLIGHT:
      if(ltick < ledLight[i]){
        temp |= 1;
      }
      if(ltick == 1){
        ledcontrol[i]++;
        if(ledcontrol[i] & (1<<DIMMSPEED)){
          ledcontrol[i] &= ~63;
        if(ledcontrol[i] & 64){
          ledLight[i] += (ledcontrol[i] & 128)?1:-1;
          if(ledLight[i] & (1<<STEPS)){
            ledcontrol[i] &= ~128;
          }
          if(!ledLight[i]){
            ledcontrol[i] &= ~64;
          }
        }else{
          if(RandomChar() > RANDOMSPEED &&
             RandomChar() > RANDOMSPEED &&
             RandomChar() > RANDOMSPEED &&
             RandomChar() > RANDOMSPEED){
            ledcontrol[i] |= 64 | 128;
          }
        }
      }
      }
        break;
    }
    if(i < 31)
      temp <<= 1;
  }
  return temp;
}

/**
* Initializes the timer 0.
*/
void InitTimer(void){
   TCCR0A |= WGM01;
   OCR0A = 1;
   TCCR0B |= (1<<CS00);
   TIMSK |= (1<<OCIE0A);
}

/**
* Initializes PORTB 0-2 as outputs.
*/
void InitOutputs(void){
   DDRB |= (1<<PB0) | (1<<PB1) | (1<<PB2);
}

/**
* Writes an LEDstate to the shift registers.
*/
void WriteState(LEDstate state){
   PORTB &= ~(1<<PB1);
   for(char i = 0; i < 32; i++){
      PORTB &= ~((1<<PB0)|(1<<PB2));
      if(state & (1L<<i)){
        PORTB |= (1<<PB0);
      }
      PORTB |= (1<<PB2);
   }
   PORTB |= (1<<PB1);
}

/**
* Checks and corrects ledLight values.
*/
void CheckConfig(){
  for(unsigned char i = 0; i < 32; i++){
    ledcontrol[i] = 0;
    switch (states[i]){
      case ON:
        ledLight[i]%=(1<<STEPS);
        break;
      case OFF:
        ledLight[i] = 0;
        break;
      case FULLWAVE:
       ledLight[i]%=(1<<STEPS);
        break;
      case VARIATE:
        ledLight[i]%=(1<<STEPS);
        if(ledLight[i] < LEDMIN){
          ledLight[i] = LEDMIN;
        }
        break;
      case HIGHLIGHT:
        ledLight[i] = 0;
        break;
    }
  }
}

/**
* Generates a pseudo random char.
* Yes I know, not the best solution, but it seems to works, so...
*/
unsigned char RandomChar(){
  seed ^= seed << 1;
	seed |= seed >> 1;
	seed += seed & (seed & 3);
  return (unsigned char) (255 & (seed >> 8));
}

# LED_Canvas
A little program for the Attiny85 that can control 32 LEDs with different modes.

Use `make flash` to compile and upload.

To customize the LEDs configure the `CONFIGURE HERE` section.

Works with 4 74hc595 8bit shift registers.

| First Shift Register  | Attiny85 PORT |
| ------------- | ------------- |
| Serial in (14)  | PB0  |
| RCLK (12)  | PB1  |
| SRCLK (11) | PB2  |

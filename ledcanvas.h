/**
 * An LEDstate is a 32bit storage that saves what LEDs should be on.
 */
typedef unsigned long int LEDstate;

/**
 * The LEDmodes are the different modes a LED can have.
 */
typedef enum {OFF = 0, ON = 1, FULLWAVE = 2, VARIATE = 3, HIGHLIGHT = 4} LEDmode;

/**
 * Prototypes
 */

/**
 * Initializes Timer 0.
 */
void InitTimer(void);

/**
 * Initializes PORTB 0-2 as outputs.
 */
void InitOutputs(void);

/**
 * Writes an LEDstate to the shift registers.
 */
void WriteState(LEDstate state);

/**
 * Generates a pseudo random char.
 */
unsigned char RandomChar();

/**
 * Calculates the next LEDstate.
 */
LEDstate GenerateNextState(unsigned char ltick);

/**
 * Checks and corrects ledLight values.
 */
void CheckConfig();
